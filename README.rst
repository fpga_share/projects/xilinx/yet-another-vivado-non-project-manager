Template Vivado non-project mode project
###############################################################################
The script is run via the **./run.sh** command. 

The project configuration is set in the config.yaml file. The file contains all
the necessary parameters to build the project in Vivado, and to run modeling
tools (support for xsim(Vivado) and Verilator is planned). After the parameters
project parameters are described in the yaml config, you can start the
procedures of building and modeling. CLI interface is used to run commands.

Managing the config file
===========================
At the first call it is necessary to create a configuration file. You can use
the template generator:

.. code-block:: sh

   ./run.sh -C

Example of a configuration file:

.. code-block:: yaml

   #
   # Common parameters
   #
   common:
       vivado_bindir_pname: "/path/to/vivado_bin_dir"
       
   #
   # Vivado project parameters
   #
   vivado:
       # FPGA part number
       fpga_partnum: xc7k325tffg676-1
       # Name of top hdl file
       top_name: my_top
       # hw_server ip address. If connecting to a local server, the value should 
       # be "localhost"
       hw_server_ip: localhost
       # Port hw_server. The default is 3121
       hw_server_port: 3121
       # JTAG bus frequency
       jtag_freq_hz: 15000000

       files:
           # Project source files
           hdl:
               - src/top/my_v_file.v
               - src/top/my_sv_file.sv
               - src/lib/my_lib_v_file.v
               - src/lib/*.v
           # XDC files
           xdc:
               - src/constraints/my_file.xdc
               - src/constraints/my_another_file.xdc
               - src/constraints/*.xdc

Next, you need to fill in the configure fields with your data:

* **common::vivado_bindir_pname** - path name of the directory with vivado
  executable files, vivado must be installed in the system;

* **vivado::fpga_partnum** - partnuber of the used FPGA;
* **vivado::top_name** - name of the top module of the project (the module
  itself, not the file);
* **vivado::hw_server_ip** - IP address of the *hw_server* server, which
  controls the programmer connected to the target board. programmer connected to
  the target board. If it is assumed that the board is connected locally to the
  current computer, it is necessary to leave "localhost";
* **vivado::hw_server_port** - network port of *hw_server*, which controls the
  programmer connected to the target board. programmer connected to the target
  board. If it is assumed that the board is connected locally to the current
  computer, it is necessary to leave "3121";
* **vivado::jtag_freq_hz** - frequency of JTAG bus of the programmer;
* **vivado::files::hdl** - list of Verilog/SystemVerilog files of the project.
  You can specify:
    * full relative (project root) path to the file;
    * mask "path/*.v" or "path/.sv". The path must be relative to the project
      root of the project. The build script will automatically (recursively)
      find all v/sv files below the specified path.
  It is allowed to duplicate file connection with full path command, and path by
  mask. In the final list of files all duplicated paths will be cleared;
* **vivado::files::xdc** - list of constrains - XDC files of the project. You
  can specify:
    * full relative (project root) path to the file;
    * mask "path/*.xdc". The path must be relative to the project root. Assembly
      script will automatically (recursively) find all xdc files below the
      specified path. path.
  It is allowed to duplicate the file connection with the full path command, and
  the path p

Using CLI
==========================================

Building project(Vivado)
------------------------------------------
The following CLI arguments are currently available:

* **-a|--all** - run the full project build cycle. This involves running
  synthesis, placement and routing, generation of build artifacts (control
  points (dcp), ltx file (if ILA is enabled), bits file (if ILA is enabled) will
  be created);

* **-l|--lint** - start linter. This command allows you to quickly check
  correctness of project description by Vivado;

* **-s|-synthesis** - start synthesis. After a successful start the project
  state will be saved to the counter point file (dcp), at the path:
  "build/vivado/prj/post_synth.dcp". After that, if necessary, it will be
  possible to open this file in the Vivado GUI to view the synthesized
  circuitry, add ILAs, IP blocks, or other operations (see the description of
  the GUI start command). After starting GUI mode, you will need to open the
  checkpoint. This can be done by calling: "File->Checkpoint->Open";

  .. note::

     After editing a project in GUI mode it is necessary to save the project
     state. To do this you need to resave checkpoint: "File->Checkpoint->Save"

* **-r|--reroute** - start building the whole project, without starting
  synthesis. This is convenient for cases when after the synthesis stage edits
  have been made to the project in GUI mode;

* **-p|--pnr** - start the Place, Routing stages (after each stage a different
  checkpoint will be saved); 

* **-b|--bin** - form build artifacts (it makes sense only if the steps of
  Synthesis and Routing have been performed);

* **-f|--flash** - flash the FPGA. At this point it is assumed that there is
  only one programmer connected to the PC; 

* **-g|--gui** - start Vivado in GUI mode;

* **--gen_bd** - generate BlockDesign project;

  .. note::

     The option is MUST if used in a BlockDesign project. If the script with the
     ``-a|--all`` or ``-s`` switch is run, generation will occur automatically.
     In all other cases it is necessary to call this command manually

* **--postsynth_cmd** - read the post-synthesis checkpoint and execute an
  arbitrary vivado command. For example, to get a report on clocks:

  .. code-block:: tcl

     ./run.sh viv --postsynth_cmd report_clocks

* **--postplace_cmd** - read the post-place checkpoint and execute an
  arbitrary vivado command;

* **--postroute_cmd** - read the post-place checkpoint and execute an
  arbitrary vivado command.

Simulation
------------------------------------------

TODO
==========================================
* Separate scripts from the project so that they can be used as a submodule in a
  wrapper project that includes a separate submodule with hdl project and a
  separate submodule with scripts;
* Add VCD file generation;
