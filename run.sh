#! /bin/bash

THIS_DIR=$(dirname $(realpath -s $0))
MANAGER_DIR=$(dirname $(realpath $0))
VENV_DIR=$THIS_DIR/.venv

# if .venv does not exist, create it
if [[ ! -d $VENV_DIR ]]; then
    python3 -m venv .venv
    if [[ $? != 0 ]]; then
        echo "Error! Unable to create .venv dir. Exiting"
        exit 1
    fi
fi

# activate venv
source $VENV_DIR/bin/activate

# If there is a file with the required packages - install them
REQ_FILE=$THIS_DIR/requirements.txt
if [[ -f $REQ_FILE ]]; then
    pip3 install -r $REQ_FILE
    if [[ $? != 0 ]]; then
        echo "Error! Failed to install dependencies. Exiting"
        exit 1
    fi
fi

RUNNER="${MANAGER_DIR}/scripts/main.py"
${RUNNER} -c $THIS_DIR/config.yaml $@

# venv shell can not be deactivated, because it exists only in a fork of the
# parent shell, and will be automatically destroyed on exit
