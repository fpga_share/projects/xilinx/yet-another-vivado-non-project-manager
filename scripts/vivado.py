import os
import re
import json
from pathlib import Path

import aux.exceptions as exceptions
import launch_env
from aux.futils import FUtils
from gpop_actions import GenPopParams
from aux.term_utils import TermColors
from aux.prog_runner import ProgRunner
from src_searcher import SrcSearcher


###############################################################################
# Vivado run configs
###############################################################################
class VivadoRunnerConfig:
    ''' Configs for Vivado project
    '''
    fpga_partnum    : str
    '''FPGA part number'''
    vdefs           : list[dict]
    '''verilog defines list'''
    top_module_name : str
    '''Vivado prj top module name'''
    hdl_src         : list[str]
    '''List of pathnames of hdl sources'''
    constr_src      : list[str]
    '''List of path names of verilog sources'''
    bd_src          : list[str]
    '''List of path names of BlockDesign sources'''

    hw_server_ip    : str
    '''Service hwserver address '''
    hw_server_port  : str
    '''Service hwserver port '''
    jtag_freq_hz    : str
    '''JTAG bus frequency in Hz'''

    run_lint        : bool
    run_synth       : bool
    run_reroute     : bool
    run_pnr         : bool
    run_all         : bool
    mk_bin          : bool
    flash           : bool
    run_gui         : bool
    run_tcl         : bool
    gen_bd          : bool
    postsynth_cmd   : list[str] | None
    postplace_cmd   : list[str] | None
    postroute_cmd   : list[str] | None


class _CmdBook:

    def __init__(
          self
        , proot_dir : str
        , vivado_param : VivadoRunnerConfig
        ):
        self.prootd = proot_dir
        self.vparam = vivado_param
        self.report_d = os.path.join(proot_dir, launch_env.vivado_reports_dir)
        self.build_d = os.path.join(proot_dir, launch_env.vivado_gen_dir)
        self.binart_d = os.path.join(proot_dir, launch_env.vivado_binart_dir)
        self.fileset_name = "src_fileset"

    def preambule(self) -> str:
        return '''\
###############################################################################
# WARNING: This file is created automatically
# Any manual changes made will be lost after the next start of the build system
###############################################################################
'''

    def log_procs(self) -> str:
        return '''
#
# @brief Function for displaying a error message
#
proc err {args} {
    puts "ERROR: $args"
}

#
# @brief Function for displaying a info message
#
proc msg {args} {
    puts "INFO: $args"
}

#
# @brief Function for displaying a warning message
#
proc warn {args} {
    puts "WARNING: $args"
}
'''

    def set_fpga_part(self):
        return f'''
#
# Set FPGA part
#
set_part {self.vparam.fpga_partnum}
        '''

    def verilog_defines(self) -> str:
        defs: list[dict] = self.vparam.vdefs
        ret_string = ""
        if defs is not None:
            for pairs in defs:
                key = [*pairs.keys()][0]
                ret_string += \
                f" -verilog_define \"{key}={pairs[key]}\" "
        return ret_string

    def read_v_src(self, file: str) -> str:
        return f"read_verilog {file}\n"

    def read_sv_src(self, file: str) -> str:
        return f"read_verilog -sv {file}\n"

    def read_xdc_src(self, file: str) -> str:
        return f"read_xdc -unmanaged {file}\n"

    def read_bd_src(self, file: str) -> str:
        return f'''
read_bd {file}\n
set_property synth_checkpoint_mode None [get_files {file}]
'''

    def generate_bd_src(self, file: str) -> str:
        return f'''
# read_bd {file}\n
set_property synth_checkpoint_mode None [get_files {file}]
generate_target all [get_files {file}]
'''

    def run_gui(self) -> str:
        return f"start_gui\n"

    def _synth_cmd(self) -> str:
        return f"synth_design -top {self.vparam.top_module_name} {self.verilog_defines()}"

    def run_linter(self) -> str:
        return self._synth_cmd() + " -lint "

    def run_synth(self) -> str:
        return f'''
#
# add Synthesis commands
#
{self._synth_cmd()}
write_checkpoint -force {self.build_d}/{launch_env.vivado_postsynt_chkp}
report_timing_summary -file {self.report_d}/post_synth_timing_summary.rpt
report_ip_status -file {self.report_d}/post_synth_ip_status_summary.rpt
report_power -file {self.report_d}/post_synth_power.rpt
report_clock_interaction            \\
        -delay_type min_max         \\
        -file {self.report_d}/post_synth_clock_interaction.rpt
report_clocks                       \\
        -file {self.report_d}/post_synth_report_clocks.rpt
report_high_fanout_nets             \\
        -fanout_greater_than 200    \\
        -max_nets 50                \\
        -file {self.report_d}/post_synth_high_fanout_nets.rpt
'''

    def run_placement(self) -> str:
        return f'''
#
# add Placement commands
#
opt_design
place_design
phys_opt_design
write_checkpoint -force {self.build_d}/post_place
report_timing_summary -file {self.report_d}/post_place_timing_summary.rpt
'''

    def run_routing(self) -> str:
        return f'''
#
# add Routing commands
#
route_design
write_checkpoint -force {self.build_d}/{launch_env.vivado_postroute_chkp}
report_timing_summary -file {self.report_d}/post_route_timing_summary.rpt
report_timing                 \\
      -max_paths 100          \\
      -path_type summary      \\
      -slack_lesser_than 0    \\
      -file {self.report_d}/post_route_setup_timing_violations.rpt
report_clock_utilization -file {self.report_d}/clock_util.rpt
report_utilization -file {self.report_d}/post_route_util.rpt
report_power -file {self.report_d}/post_route_power.rpt
report_drc -file {self.report_d}/post_imp_drc.rpt
write_verilog -force {self.build_d}/impl_netlist.v
write_xdc                       \\
      -no_fixed_only            \\
      -force {self.build_d}/impl.xdc
'''

    def mk_bin_arts(self) -> str:
        return f'''
#
# add mk bin artefacts commands
#
write_bitstream      -force {self.binart_d}/{self.vparam.top_module_name}.bit
write_debug_probes   -force {self.binart_d}/{self.vparam.top_module_name}.ltx
'''

    def run_reroute(self, postsynth_chkp_pname : str) -> str:
        return f'''
read_checkpoint {postsynth_chkp_pname}
link_design -top {self.vparam.top_module_name}
{self.run_placement()}
{self.run_routing()}
{self.mk_bin_arts()}
'''

    def run_all(self) -> str:
        return f'''
{self.run_synth()}
{self.run_placement()}
{self.run_routing()}
{self.mk_bin_arts()}
'''

    def flash(
          self
        , bitstream_pname : str
        , probe_pname : str = None
        ) -> str:
        cmd = f'''
#
# Flash FPGA commands
#
open_hw_manager
connect_hw_server -url {self.vparam.hw_server_ip}:{self.vparam.hw_server_port}
set hwt [lindex [get_hw_targets *] 0]
current_hw_target $hwt
set_property PARAM.FREQUENCY {self.vparam.jtag_freq_hz} $hwt
open_hw_target $hwt

puts "INFO: FPGA type - [lindex [get_hw_devices] 0]"

current_hw_device [lindex [get_hw_devices] 0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 0]

set_property PROGRAM.FILE {bitstream_pname} [lindex [get_hw_devices] 0]
'''
        if None != probe_pname:
            cmd = cmd + f'''
set_property PROBES.FILE {probe_pname} [lindex [get_hw_devices] 0]
set_property FULL_PROBES.FILE {probe_pname} [lindex [get_hw_devices] 0]
'''
        cmd = cmd + f''' 
puts "INFO: Programming started"
program_hw_devices [lindex [get_hw_devices] 0]
refresh_hw_device  [lindex [get_hw_devices] 0]
puts "INFO: Programming completed"

close_hw_target $hwt
disconnect_hw_server

config_webtalk -user off
close_hw_manager
'''
        return cmd

    def open_chkp_and_run_cmd(self, chkp: str, cmd: str) -> str:
        """Read seleceted check point, and run command"""
        return f'''
open_checkpoint {self.build_d}/{chkp}
puts "INFO: run user command '{cmd}:"
{cmd}
'''

class VivadoColorizeLog:

    @staticmethod
    def colorize(line : str) -> str:
        termclr = TermColors()

        info_str = "INFO:"
        clr_info_str = termclr.green_bold_str(info_str)
        line = re.sub(info_str, clr_info_str, line)

        warn_str = "WARNING:"
        clr_warn_str = termclr.blue_bold_str(warn_str)
        line = re.sub(f"^{warn_str}", clr_warn_str, line)

        crit_warn_str = "CRITICAL WARNING:"
        clr_critwarn_str = termclr.violet_bold_str(crit_warn_str)
        line = re.sub(crit_warn_str, clr_critwarn_str, line)

        err_str = "ERROR:"
        clr_err_str = termclr.red_bold_str(err_str)
        line = re.sub(err_str, clr_err_str, line)

        return line

class Vivado:

    def __init__(
          self
        , logger
        , viv_params    : VivadoRunnerConfig
        , main_params   : GenPopParams
        ):
        self.logger     = logger
        self.proot_dir  = main_params.main_prj_path
        self.main_param = main_params
        self.viv_params = viv_params
        self.futils     = FUtils(logger)
        self.cmd_book   = _CmdBook(self.proot_dir, viv_params)
        self.src_srchr  = SrcSearcher(logger, self.proot_dir)

        self.vivado_scr = os.path.join(self.proot_dir, launch_env.vivado_scr)
        self.log_d = os.path.join(self.proot_dir, launch_env.vivado_logs_dir)
        self.tcl_scr = os.path.join(self.proot_dir, launch_env.vivado_scr)

        self.need_run_viv_backgr = False


    def _run_vivado(
            self,
            need_run_viv_backgr : bool,
            run_tcl_term : bool
        ) -> int:
        vivado_bin = os.path.join(self.main_param.vivado_bindir_pname, "vivado")
        _runner     = ProgRunner()
        if run_tcl_term:
            vivado_cmd_mode = "-mode tcl"
        else:
            vivado_cmd_mode = f"-mode batch -source {self.tcl_scr}"

        vivado_cmd = f'''
        {vivado_bin}                            \
            -journal {self.log_d}/jou           \
            -log     {self.log_d}/log           \
            -notrace                            \
            {vivado_cmd_mode}
        '''
        if need_run_viv_backgr:
            runner = _runner.run_sytem_cmd_detached
        else:
            _runner.set_colorizer(VivadoColorizeLog.colorize)
            runner = _runner.run_sytem_cmd

        ret = runner(vivado_cmd)
        if type(ret) == list:
            if ret[0]:
                self.logger.error(
                    f"Vivado terminated with an error!\n"
                    )
                return ret[0]
        if need_run_viv_backgr:
            self.logger.info("Wait a few seconds for the GUI to start...")

        return 0

    @staticmethod
    def run_vivado(
            vivado_bin: str,
            vivado_tcl_scr_pname: str,
            logger,
            need_run_viv_backgr : bool,
            run_tcl_term : bool,
            log_dir: str
        ) -> int:

        if run_tcl_term:
            vivado_cmd_mode = "-mode tcl"
        else:
            vivado_cmd_mode = f"-mode batch -source {vivado_tcl_scr_pname}"

        vivado_cmd = f'''
        {vivado_bin}                            \
            -journal {log_dir}/jou              \
            -log     {log_dir}/log              \
            -notrace                            \
            {vivado_cmd_mode}
        '''

        _runner     = ProgRunner()
        if need_run_viv_backgr:
            runner = _runner.run_sytem_cmd_detached
        else:
            _runner.set_colorizer(VivadoColorizeLog.colorize)
            runner = _runner.run_sytem_cmd

        ret = runner(vivado_cmd)
        if type(ret) == list:
            if ret[0]:
                logger.error(
                    f"Vivado terminated with an error!\n"
                    )
                return ret[0]
        if need_run_viv_backgr:
            logger.info("Wait a few seconds for the GUI to start...")

        return 0

    def _read_prj_files(self) -> str:
        '''
        Create a line with vivado tcl commands connecting all project files

        Return
        ---------
        tcl commands for Vivado
        '''
        vh_list, v_list, svh_list, sv_list = self.src_srchr.create_hdl_files_lists(
                                                    self.viv_params.hdl_src
                                                    )
        xdc_list = self.src_srchr.create_xdc_files_list(
                                                    self.viv_params.constr_src
                                                    )
        bd_list = self.src_srchr.create_bd_files_list(
                                                    self.viv_params.bd_src
                                                    )

        cmd_str = '''
#
# add sources
#
'''
        v_files = vh_list + v_list
        for item in v_files:
            cmd_str += self.cmd_book.read_v_src(item)

        sv_files = svh_list + sv_list
        for item in sv_files:
            cmd_str += self.cmd_book.read_sv_src(item)

        for item in xdc_list:
            cmd_str += self.cmd_book.read_xdc_src(item)

        for item in bd_list:
            cmd_str += self.cmd_book.read_bd_src(item)

        return cmd_str

    def __correct_genpath_in_bd(self, bd_file) -> None:
        target_pname = os.path.join(
              self.proot_dir
            , launch_env.vivado_build_bd_dir
            , Path(bd_file).stem
            )
        j_dump = None
        try:
            with open(bd_file, "r") as file:
                j_dump = json.load(file)
        except:
            self.logger.error(
                f"The specified bd file ({bd_file}) was not found"
                )
            raise exceptions.BadConfig()
        try:
            j_dump['design']['design_info']['gen_directory'] = target_pname
        except KeyError:
            self.logger.warning(
                f"In the BlockDeisgn file \"{bd_file}\" no field with the "
                f"path of saving generated sources was found."
                )
            return

        jsonData = json.dumps(j_dump, indent=2)

        try:
            with open(bd_file, "w") as file:
                file.write(jsonData)
        except:
            self.logger.error(
                f"Cannot overwrite a bd file ({bd_file}) with the save path "
                f"parameter changed "
                )
            raise exceptions.BadConfig()

    def _gen_bd_files(self) -> str:
        '''
        Bypass the list of BlockDesign files in the project, and generate hdl
        sources for each of them

        Return
        ---------
        tcl commands for Vivado
        '''

        bd_list = self.src_srchr.create_bd_files_list(
                                                    self.viv_params.bd_src
                                                    )
        cmd_str = ""
        for item in bd_list:
            self.__correct_genpath_in_bd(item)
            cmd_str += self.cmd_book.generate_bd_src(item)

        return cmd_str

    def _writecmd_read_chkp_and_run_command(
                                      self
                                    , chkp_name: str
                                    , viv_params_cmd: list[str]
                                    ):
        """
        """
        cmd_str = ""
        for word in viv_params_cmd:
            cmd_str += f"{word} "

        cmd = self.cmd_book.open_chkp_and_run_cmd(
                                  chkp_name + ".dcp"
                                , cmd_str
                                )
        with open(self.vivado_scr, 'a') as file:
            file.write(self._read_prj_files())
            file.write(cmd)


    def _mk_tcl_script(self) -> bool:
        with open(self.vivado_scr, 'w') as file:
            file.write(self.cmd_book.preambule())
            file.write(self.cmd_book.log_procs())
            file.write(self.cmd_book.set_fpga_part())

        if self.viv_params.run_gui:
            self.need_run_viv_backgr = True
            with open(self.vivado_scr, 'a') as file:
                file.write(self._read_prj_files())
                file.write(self.cmd_book.run_gui())
            return True

        if self.viv_params.run_tcl:
            self.need_run_viv_backgr = False
            return True

        if self.viv_params.gen_bd:
            with open(self.vivado_scr, 'a') as file:
                file.write(self._read_prj_files())
                file.write(self._gen_bd_files())
            return True

        if self.viv_params.run_lint:
            with open(self.vivado_scr, 'a') as file:
                file.write(self._read_prj_files())
                file.write(self.cmd_book.run_linter())
            return True

        if self.viv_params.run_synth:
            with open(self.vivado_scr, 'a') as file:
                file.write(self._read_prj_files())
                file.write(self._gen_bd_files())
                file.write(self.cmd_book.run_synth())
            return True

        if self.viv_params.run_reroute:
            postsynth_chkp_pname = os.path.join(
                  self.proot_dir
                , launch_env.vivado_postsynt_chkp
                ) + ".dcp"
            self.futils.check_file(postsynth_chkp_pname)
            with open(self.vivado_scr, 'a') as file:
                file.write(self._read_prj_files())
                file.write(self.cmd_book.run_reroute(postsynth_chkp_pname))
            return True

        if self.viv_params.run_pnr:
            with open(self.vivado_scr, 'a') as file:
                file.write(self._read_prj_files())
                file.write(self.cmd_book.run_placement())
                file.write(self.cmd_book.run_routing())
            return True

        if self.viv_params.run_all:
            with open(self.vivado_scr, 'a') as file:
                file.write(self._read_prj_files())
                file.write(self._gen_bd_files())
                file.write(self.cmd_book.run_all())
            return True

        if self.viv_params.flash:
            bitsream = os.path.join(
                  self.proot_dir
                , launch_env.vivado_binart_dir
                , f"{self.viv_params.top_module_name}.bit"
                )
            self.futils.check_file(bitsream)

            probe = os.path.join(
                  self.proot_dir
                , launch_env.vivado_binart_dir
                , f"{self.viv_params.top_module_name}.ltx"
                )
            need_probe = self.futils.check_file(
                                              probe
                                            , raise_if_err = False
                                            , logging = False
                                            )
            if not need_probe:
                probe = None

            with open(self.vivado_scr, 'a') as file:
                file.write(self._read_prj_files())
                file.write(self.cmd_book.flash(bitsream, probe))
            return True


        if self.viv_params.postsynth_cmd:
            self._writecmd_read_chkp_and_run_command(
                                  chkp_name=launch_env.vivado_postsynt_chkp
                                , viv_params_cmd=self.viv_params.postsynth_cmd
                                )
            return True

        if self.viv_params.postplace_cmd:
            self._writecmd_read_chkp_and_run_command(
                                  chkp_name=launch_env.vivado_postplace_chkp
                                , viv_params_cmd=self.viv_params.postplace_cmd
                                )
            return True

        if self.viv_params.postroute_cmd:
            self._writecmd_read_chkp_and_run_command(
                                  chkp_name=launch_env.vivado_postroute_chkp
                                , viv_params_cmd=self.viv_params.postroute_cmd
                                )
            return True

        return False


    def run(self) -> int:
        """ Run vivado

            Return: 0 if OK, else: -1
        """
        rc = self._mk_tcl_script()
        if rc:
            vivado_bin = os.path.join(
                                self.main_param.vivado_bindir_pname,
                                "vivado"
                                )
            # return self._run_vivado(
            #         need_run_viv_backgr=self.need_run_viv_backgr,
            #         run_tcl_term=self.viv_params.run_tcl
            #         )
            return self.run_vivado(
                    logger=self.logger,
                    vivado_bin=vivado_bin,
                    vivado_tcl_scr_pname=self.tcl_scr,
                    need_run_viv_backgr=self.need_run_viv_backgr,
                    run_tcl_term=self.viv_params.run_tcl,
                    log_dir=self.log_d
                    )
        return -1
