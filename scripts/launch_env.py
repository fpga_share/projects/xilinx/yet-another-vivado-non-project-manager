###############################################################################
# Main
###############################################################################
user_conf_fname = "config.yaml"
'''Name of the file with user config (relative to the project root)'''

user_cfg_template = "scripts/configs/user_cfg_template.yaml"
'''Pathname of the config template'''

###############################################################################
# Vivado
###############################################################################
vivado_gen_dir = "build/vivado/prj"
'''Relative path to the directory with generated project files'''

vivado_reports_dir = "build/vivado/reports"
'''Relative path to the directory with generated retports'''

vivado_logs_dir = "build/vivado/logs"
'''Relative path to the directory with vivado logs'''

vivado_build_bd_dir = "build/vivado/bd"

vivado_binart_dir = "build/vivado/arts"
'''Relative path to the directory with generated project artifacts (bitsream,
   ltx, e.t.c.)
'''
vivado_scr = f"{vivado_gen_dir}/main_script.tcl"
'''Automatically generated startup script'''

vivado_postsynt_chkp  = "post_synth"
'''Name of post-synthesis checkpoint'''

vivado_postplace_chkp = "post_place"
'''Name of post-place checkpoint'''

vivado_postroute_chkp = "post_rounte"
'''Name of post-route checkpoint'''

###############################################################################
# Xsim
###############################################################################
xsim_dir = "build/xsim"
'''Relative path to the directory with xsim files'''

xsim_arts_dir = "build/xsim/arts"
'''Relative path to the directory with generated project files'''

xsim_logs_dir = "build/xsim/logs"
'''Relative path to the directory with vivado logs'''

xsim_worklib = "simlib"
