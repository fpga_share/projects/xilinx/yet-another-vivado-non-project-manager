#! /usr/bin/python3

import os
import sys
import aux.logger as logger

# путь к корневой директории проекта
mroot_dir = os.path.join(
                  os.path.dirname(
                      os.path.abspath(sys.argv[0])
                      )
                , "../"
                )
mroot_dir = os.path.abspath(mroot_dir)

sys.path.insert(0, os.path.join(mroot_dir, "scripts", 'aux') )
sys.path.insert(0, os.path.join(mroot_dir, "scripts", 'configs') )

logger  = logger.CustomLogger()

if 1 == len(sys.argv):
    logger.error(
        "No commands transferred, don't know what to do... I'm done."
        )
    exit(1)

###############################################################################
# Run...
###############################################################################
import aux.exceptions as exceptions
import configs.parameterizer as parameterizer
import gpop_actions
import vivado
import sim
try:
    params = parameterizer.Parameterizer(
                              logger   = logger
                            , mroot_dir = mroot_dir
                            ).params
except exceptions.BadConfig:
    exit(1)

gpop_actions = gpop_actions.Actions(
      logger    = logger
    , params    = params.main
    )

if type(params.applic) is vivado.VivadoRunnerConfig:

    try:
        gpop_actions.create_viv_build_dirs()
    except (exceptions.BadOp, KeyboardInterrupt):
        exit(1)

    vivado = vivado.Vivado(
          logger        = logger
        , viv_params    = params.applic
        , main_params   = params.main
        )
    try:
        ret = vivado.run()
    except exceptions.BadConfig:
        exit(1)
    except KeyboardInterrupt:
        logger.error(
            "Operation aborted by user "
            )
        exit(1)
    exit(ret)

if type(params.applic) is sim.SimRunnerConfig:
    try:
        gpop_actions.create_xsim_build_dirs()
    except exceptions.BadOp:
        exit(1)
    except KeyboardInterrupt:
        logger.error(
            "Operation aborted by user "
            )
        exit(1)

    sim = sim.Sim(
          logger        = logger
        , sim_params    = params.applic
        , main_params   = params.main
        )
    try:
        ret = sim.run()
        if ret:
            logger.error(f"The simulation ended with an error!")
    except (exceptions.BadOp, KeyboardInterrupt):
        exit(1)
    exit(ret)

