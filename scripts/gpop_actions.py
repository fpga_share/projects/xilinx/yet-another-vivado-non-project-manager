import os
import launch_env
import aux.futils as futils
import aux.exceptions as exceptions
#
# Параметры общего назначения
#
class GenPopParams:
    mk_user_cfg_templ       : bool
    '''создать шаблон с конфигом пользователя'''
    vivado_bindir_pname     : str
    '''pathname of vivado bin dir'''
    used_config_pname       : str
    '''Name of the config file used in this session'''
    main_prj_path           : str
    '''Path to the root directory of the top project that includes this as a
       module
    '''

class Actions:

    def __init__(
          self
        , logger
        , params
        ):

        self.logger     = logger
        self.params     = params
        self.futils     = futils.FUtils(logger = logger)


    def create_viv_build_dirs(self):
        gen_dir = os.path.join(
              self.params.main_prj_path
            , launch_env.vivado_gen_dir
            )
        if not self.futils.make_dir(gen_dir):
            raise exceptions.BadOp()

        binart_dir = os.path.join(
              self.params.main_prj_path
            , launch_env.vivado_binart_dir
            )
        if not self.futils.make_dir(binart_dir):
            raise exceptions.BadOp()

        reports_dir = os.path.join(
              self.params.main_prj_path
            , launch_env.vivado_reports_dir
            )
        if not self.futils.make_dir(reports_dir):
            raise exceptions.BadOp()

        logs_dir = os.path.join(
              self.params.main_prj_path
            , launch_env.vivado_logs_dir
            )
        if not self.futils.make_dir(logs_dir):
            raise exceptions.BadOp()


    def create_xsim_build_dirs(self):
        arts_dir = os.path.join(
              self.params.main_prj_path
            , launch_env.xsim_arts_dir
            )
        if not self.futils.make_dir(arts_dir):
            raise exceptions.BadOp()

        logs_dir = os.path.join(
              self.params.main_prj_path
            , launch_env.xsim_logs_dir
            )
        if not self.futils.make_dir(logs_dir):
            raise exceptions.BadOp()
