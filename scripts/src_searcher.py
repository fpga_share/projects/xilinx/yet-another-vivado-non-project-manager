import os
import re

from aux.futils import FUtils

class SrcSearcher:
    def __init__(self, logger, root_dir):
        self.logger = logger
        self.root_dir = root_dir
        self.futils = FUtils(logger)

    def _find_files(self, path_list: list[str], mask_extens: str) -> list[str]:
        '''
        Find files from a list in the file system

        Parameters
        -----------
        path_list: list of file paths
        mask_extens: list of file paths

        Return
        -------
        List of files

        '''
        file_list : list[str] = []
        for path in path_list:
            path = os.path.join(self.root_dir, path)

            # found recursive all *. files from list
            if re.search(f"\*\.{mask_extens}$", path):
                path = os.path.dirname(path)
                file_list += self.futils.find_files(path, f"\.{mask_extens}$")
                continue

            # found file from list
            if re.search(f"\.{mask_extens}$", path):
                self.futils.check_file(path)
                file_list.append(path)
                continue

        # delete duplicates from lists
        file_list = list(dict.fromkeys(file_list))
        return file_list

    def create_hdl_files_lists(self, path_list) -> tuple[list[str], list[str],
        list[str], list[str]]:
        '''
        Create lists of v/sv filenames of project files

        Parameters
        -----------
        path_list: list of file paths or search masks

        Return
        -------
        Tuple from lists of *.v and *.sv files (v,sv)

        '''
        vh_list : list[str] = []
        v_list : list[str] = []
        svh_list : list[str] = []
        sv_list : list[str] = []

        vh_list = self._find_files(path_list, "vh")
        v_list = self._find_files(path_list, "v")
        svh_list = self._find_files(path_list, "svh")
        sv_list = self._find_files(path_list, "sv")
        return (vh_list, v_list, svh_list, sv_list)

    def create_xdc_files_list(self, path_list) -> list[str]:
        '''
        Create list of xdc filenames of project files

        Parameters
        -----------
        path_list: list of file paths or search masks

        Return
        -------
        List of *.xdcfiles

        '''

        return self._find_files(path_list, "xdc")

    def create_bd_files_list(self, path_list) -> list[str]:
        '''
        Create list of BlockDesigner filenames of project files

        Parameters
        -----------
        path_list: list of file paths or search masks

        Return
        -------
        List of files

        '''
        return self._find_files(path_list, "bd")
