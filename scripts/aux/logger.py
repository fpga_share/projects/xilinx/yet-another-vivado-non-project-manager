""" Кастомный логгер

Модуль-обертка над конкретным логгером

"""

import logging
import sys

from aux.term_utils import TermColors


class LogLevels:
    """ Уровни логгирования

    Класс содержит перечисление доступных уровней логгирования (в порядке
    возрастания:

        - Debug
        - Info
        - Warning
        - Error
        - Critical

    Выбранный уровень разрешает вывод в логгер сообщений начиная со своего
    уровня, и выше

    """

    Debug = logging.DEBUG
    ''' Уровень логгирования Debug - пропускает в лог все уровни логгера'''
    Info = logging.INFO
    ''' Уровень логгирования Info - пропускает в лог Info, Warn, Err, Crit'''
    Warn = logging.WARNING
    ''' Уровень логгирования Warning - пропускает в лог Warning, Err, Crit'''
    Error = logging.ERROR
    ''' Уровень логгирования Error - пропускает в лог Err, Critical'''
    Critical = logging.CRITICAL
    ''' Уровень логгирования Critical - пропускает в лог Critical'''


class CustomFormatter(logging.Formatter):
    """Кастомный форматтер для логгера, переопределяющий формат вывода"""

    # fmt = '[%(levelname)s]-[%(funcName)s::%(lineno)s] %(message)s'
    fmt = '[%(levelname)s!  %(funcName)s()  %(filename)s::%(lineno)s]  '
    colors = TermColors()

    FORMATS = {
        logging.DEBUG:    colors.yellow_bold_str,
        logging.INFO:     colors.green_bold_str,
        logging.WARNING:  colors.violet_str,
        logging.ERROR:    colors.red_str,
        logging.CRITICAL: colors.red_bold_str
        }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)(self.fmt)
        formatter = logging.Formatter(
            log_fmt + "%(message)s"
            )
        return formatter.format(record)


class CustomLogger:
    """ Сам логгер

    Класс оборачивает нижележащие библиотеки логгирования, предоставляя API

    """

    """
    переменная указывающая сколько уровней стека сверху надо отбросить при
    выводе стека на экран
    """

    def __init__(self, log_level=LogLevels.Debug):

        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(log_level)

        handler = logging.StreamHandler(stream = sys.stdout)
        self._logger.addHandler(handler)
        handler.setFormatter(
                    CustomFormatter()
                    )

        self.debug = self._logger.debug
        self.info = self._logger.info

        # номер уровня стека (относительного этого файла), который будет
        # выводится в лог
        self._stackskip = 3

    def warning(self, msg, stack_trace=False, *args, **kwargs):
        self._logger.warning(
                msg,
                stack_info = stack_trace,
                stacklevel = self._stackskip,
                *args,
                **kwargs
                )

    def error(self, msg, stack_trace=False, *args, **kwargs):
        self._logger.error(
                msg,
                stack_info = stack_trace,
                stacklevel = self._stackskip,
                *args,
                **kwargs
                )

    def critical(self, msg, stack_trace=True, *args, **kwargs):
        self._logger.critical(
                msg,
                stack_info = stack_trace,
                stacklevel = self._stackskip,
                *args,
                **kwargs
                )

