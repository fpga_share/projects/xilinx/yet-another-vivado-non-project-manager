import subprocess
import shlex


class ProgRunner:

    def __init__(self):
        self.colorizer = None

    def set_colorizer(self, colorizer):
        self.colorizer = colorizer

    def run_sytem_cmd(
          self
        , cmd: str
        , cwd: str | None = None
        , stdout_immediately: bool = True
        ):
        """
        Run cmd in system shell

        Paramenters
        ------------
        param cmd:          Command to be executed;
        stdout_immediately: Output the log immediately, while the command is
                            running, without waiting for the command to finish
        Return
        -------
        List from the return code (0 - OK), and stdout buffer
        """
        pop = subprocess.Popen(
            shlex.split(cmd),
            stderr = subprocess.STDOUT,
            stdout = subprocess.PIPE,
            text = True,
            cwd = cwd
            )
        if stdout_immediately:
            for line in pop.stdout:
                if self.colorizer:
                    print(self.colorizer(line), end='')
                else:
                    print(line, end='')
        pop.wait()
        return [pop.returncode, pop.communicate()[0]]

    def run_sytem_cmd_detached(
          self
        , cmd: str
        , cwd: str | None = None
        ):
        """
        Run cmd in system shell in detached state.
        The log and return code will not be collected in this case

        Paramenters
        ------------
        param cmd:          Command to be executed;
        """
        pop = subprocess.Popen(
            shlex.split(cmd),
            stdout = subprocess.DEVNULL,
            start_new_session = True,
            cwd = cwd
            )
        return 0
