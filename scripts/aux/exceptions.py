class BadConfig(Exception):
    '''Error in yaml config'''
    pass
class BadOp(Exception):
    '''Error in operations'''
    pass
