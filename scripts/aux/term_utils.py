""" Модуль предоставляющий средства взаимодействия с терминалом
"""


class TermColors:
    """ Класс предоставляющий визуальные эффекты для терминального вывода"""

    bold = "\x1b[1m"
    reset = "\x1b[0m"

    red = "\x1b[38;5;1m"
    blue = "\x1b[38;5;111m"
    yellow = "\x1b[38;5;226m"
    green = "\x1b[38;5;82m"
    violet = "\x1b[38;5;170m"

    def red_bold_str(self, string) -> str:
        return self.bold + self.red + string + self.reset

    def red_str(self, string) -> str:
        return self.red + string + self.reset

    def green_bold_str(self, string) -> str:
        return self.bold + self.green + string + self.reset

    def green_str(self, string) -> str:
        return self.green + string + self.reset

    def yellow_bold_str(self, string) -> str:
        return self.bold + self.yellow + string + self.reset

    def blue_bold_str(self, string) -> str:
        return self.bold + self.blue + string + self.reset

    def violet_bold_str(self, string) -> str:
        return self.bold + self.violet + string + self.reset

    def violet_str(self, string) -> str:
        return self.violet + string + self.reset

    def bold_str(self, string) -> str:
        return self.bold + string + self.reset


class TermInterractor:
    """Класс предоставляющий CLI инструменты взаимодействия с пользователем"""

    @staticmethod
    def qst(quest_messg):
        """Опрос пользователя

        Выводит в терминал строку с вопросом пользователю, ожидает ввода ответа.
        Ответ может быть либо Y|y, либо N|n. В случае если введенная
        пользователем строка не принадлежит допустимым вариантам - вопрос будет
        задан снова.

        Аргументы:

         * quest_messg - печатаемое пользователю сообщение

        Возвращаемое значение:

         * True - в случае если введен положительный ответ
         * False - в случае если введен отрицательный ответ
        """
        clr = TermColors()
        while True:
            ret = input(
                  clr.violet_str(':: ')
                + quest_messg
                + clr.green_str(" [Y|y|N|n]\n >> ")
                )
            if ret.lower() == 'y':
                print("Ok...")
                return True
            elif ret.lower() == 'n':
                print(clr.violet_str(':: Пропускаю...'))
                return False

    @staticmethod
    def qst_prio(quest_messg):
        """Опрос пользователя с приоритетом отрицательному ответу

        Выводит в терминал строку с вопросом пользователю, ожидает ввода ответа.
        Ответ может быть либо Y|y, либо N|n. В случае если введенная
        пользователем строка не принадлежит допустимым вариантам, или является
        пустой строкой - считается что пользователь ввел отрицательный ответ

        Аргументы:

         * quest_messg - печатаемое пользователю сообщение

        Возвращаемое значение:

         * True - в случае если введен положительный ответ
         * False - в случае если введен отрицательный ответ
        """
        clr = TermColors()
        while True:
            ret = input(
                clr.violet_str(':: ')
                + quest_messg
                + clr.green_str(" [N|y]\n >> ")
                )
            if ret.lower() == 'y':
                print("Ok...")
                return True
            else:
                print(clr.violet_str(':: ') + 'Пропускаю...')
                return False

    @staticmethod
    def choose_item_dialog(
              name_list
            , qst_str = "В списке имен {} элемента(ов):"
            ):
        '''
        Метод вызывающий терминальный диалог для выбора файла из списка
        Param:
            - name_list: список с именами которые нужно предложить пользователю
            - qst_str: строка с описанием количества имен. Строка обязательно
                       должна содержать {} для вывода пользователю количества
                       найденных ээлементов списка
        Ret: 
            компонент списка (имя) выбранный пользователем
        '''
        clr = TermColors()
        names_cnt = len(name_list)
        print(
              clr.violet_str(':: ')
            + qst_str.format(names_cnt)
            )
        iter = 0
        for name in name_list:
            print(clr.yellow_bold_str(f"[{iter}] {name}"))
            iter += 1

        while True:
            ret = input("Введите номер нужного имени: ")
            if not ret.isdigit():
                print(
                    clr.red_bold_str(
                        "Неверный формат строки. Введите число"
                        )
                    )
                continue
            ret = int(ret)
            if ret > (names_cnt - 1) or ret < 0:
                print(
                    clr.red_bold_str(
                        f"Неверный номер строки. Введите число "
                        f"в диапазоне [0...{names_cnt - 1}]"
                        )
                    )
                continue

            return name_list[ret]
