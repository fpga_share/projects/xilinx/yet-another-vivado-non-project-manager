""" Вспомогательный модуль с инструментами для управления/проверки файлов
"""

import os
import re
import shutil
import aux.exceptions as exceptions

class FUtils:

    def __init__(
          self
        , logger
        ):
        self.logg = logger

    @staticmethod
    def find_files(path: str, mask: str) -> list[str]:
        '''Recursively traverse all subdirectories below the specified
           subdirectory (path) and find all files containing the specified mask
           (mask)
           Parameters
           -----------
           path: the path below which all files matching the mask will be
                 searched recursively
           mask: mask by which files will be searched

           Return
           -----------
           list of founded files
        '''
        ret_list : list[str] = []
        for root, dirs, files in os.walk(path, followlinks=True):
            for file in files:
                if re.findall(mask, file):
                    ret_list.append(
                        os.path.join(root, file)
                        )
        return ret_list

    def check_dir(self, dir_pname):
        """Check the existence of the directory
        Parameters
        -----------
        dir_pname: checked directory pathname

        Exceptions
        ----------
        BadConfig, if path not exist

        """
        if not os.path.exists(dir_pname):
            self.logg.critical(
                f"Отсутствует директория {dir_pname} "
                )
            raise exceptions.BadConfig()
        if not os.path.isdir(dir_pname):
            self.logg.critical(
                f"Ожидалась директория с путевым именем \"{dir_pname}\", "
                f"однако данный файл не является директорией"
                )
            raise exceptions.BadConfig()

        return True

    def check_file(
          self
        , file_pname: str
        , raise_if_err: bool = True
        , logging: bool = True
        ) -> bool:
        """Check the existence of the regular file
        Parameters
        -----------
        file_pname: checked file pathname

        Exceptions
        ----------
        BadConfig, if file not exist

        """
        if not os.path.exists(file_pname):
            if logging:
                self.logg.error(
                    f"File \"{file_pname}\" not exist"
                    )
            if raise_if_err:
                raise exceptions.BadConfig()
            else:
                return False
        return True

    @staticmethod
    def repl_string_from_file(
          pairs_list : tuple[tuple]
        , fname : str
        ):
        """
        Заменить в файле "fname" все подстроки X на подстроки Y. 

        pairs_list: кортеж из пар вида [X,Y], где X подстрока которую нажно
                    заменить подстрокой Y;
        fname: имя файла в котором требуется заменить строки
        """

        for pairs in pairs_list:
            filedata = None
            with open(fname, 'r') as file :
              filedata = file.read()

            filedata = filedata.replace(str(pairs[0]), str(pairs[1]))

            with open(fname, 'w') as file:
              file.write(filedata)

    def make_dir(self, dir_pname):
        """
        Create a directory recursively

        Parameters
        -----------
        param dir_pname: pathname of the directory to be created;

        Return
        ------
        True if successful. In case of an error, False will be returned, and a
        a message describing the error to the logger
        """
        if not os.path.exists(dir_pname):
            try:
                os.makedirs(dir_pname, 0o755)
            except OSError as oerr:
                err = os.strerror(oerr.errno)
                self.logg.error(
                    f"Directory creation error: {err}"
                    )
                return False
            return True
        return True

    def copy_file(self, src, dest):
        """
        Копирование файла

        :param src:
        :param dest:

        :return: True/False
        """
        try:
            shutil.copy(src, dest)
        except shutil.SameFileError:
            self.logg.error(
                "Ошибка копирования файла {} в {} - файл копируется сам в себя"
                "".format(src, dest)
                )
            return False
        except OSError as oerr:
            self.logg.error(
                "Ошибка копирования файла \"{}\" в \"{}\": {}".format(
                    src,
                    dest,
                    os.strerror(
                        oerr.errno
                        ),
                    ),
                )
            return False
        return True
