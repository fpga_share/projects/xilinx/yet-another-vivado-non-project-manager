import yaml

import aux.exceptions as exceptions

###############################################################################
# Общие конфиги
###############################################################################
class CommonConfigs:
    '''Main configs'''

    def __init__(
          self
        , logger
        , yaml_sect
        ):

        self.logger = logger
        self.yaml_obj = yaml_sect

    def vivado_bindir_pname(self) -> str:
        '''Get vivado bin dir pname'''
        return self.yaml_obj['vivado_bindir_pname']

class VivadoInstrsCfgBase:
    def __init__(
          self
        , logger
        , yaml_sect
        ):

        self.logger = logger
        self.yaml_obj = yaml_sect

    def __log_and_raise(self, field_name):
        self.logger.error(
            f"[{field_name}] section not found in yaml "
            f"config. Unable to read a field from this section"
            )
        raise exceptions.BadConfig

    def __log_and_raise_file_sect(self, field_name):
        self.logger.error(
            f"['files'][{field_name}] section not found in yaml "
            f"config. Unable to read a field from this section"
            )
        raise exceptions.BadConfig

    def files_sect(self) -> list[str]:
        try:
            return self.yaml_obj['files']
        except KeyError as err:
            self.__log_and_raise(err)


###############################################################################
# Vivado synthesis and implementation configs
###############################################################################
class VivadoConfigs(VivadoInstrsCfgBase):
    '''Vivado cfg'''

    def __init__(
          self
        , logger
        , yaml_sect
        ):
        super().__init__(logger, yaml_sect)

    def fpga_partnum(self) -> str:
        '''Get FPGA part number'''
        try:
            return self.yaml_obj['fpga_partnum']
        except KeyError as err:
            self.__log_and_raise(err)

    def vdefs(self) -> list | None:
        '''Get verilog defines list'''
        try:
            vdefs = self.yaml_obj['vdefines']
        except KeyError as err:
            self.logger.info("conf field \"vdefines\" not found")
            return None
        if vdefs is None:
            self.logger.info("conf field \"vdefines\" is empty list")
            return None
        for pairs in vdefs:
            if pairs is None:
                return []
            if type(pairs) is not dict:
                self.logger.error(
                    f"item \"{pairs}\" in \"vdefines\" config fields is not "
                    "dict"
                    )
                raise exceptions.BadConfig
        return vdefs

    def top_module_name(self) -> str:
        '''Get Vivado prj top module name'''
        try:
            return self.yaml_obj['top_name']
        except KeyError as err:
            self.__log_and_raise(err)

    def hwserver_ip(self) -> str:
        '''Get Vivado hw_server ip address'''
        try:
            return self.yaml_obj['hw_server_ip']
        except KeyError as err:
            self.__log_and_raise(err)

    def hwserver_port(self) -> str:
        '''Get Vivado hw_server ip port'''
        try:
            return self.yaml_obj['hw_server_port']
        except KeyError as err:
            self.__log_and_raise(err)

    def jtag_freq_hz(self) -> str:
        '''Get JTAG bus frequency'''
        try:
            return self.yaml_obj['jtag_freq_hz']
        except KeyError as err:
            self.__log_and_raise(err)

    def hdl_sources(self) -> list[str]:
        '''Get list of pathnames of verilog sources'''
        try:
            return self.files_sect()['hdl']
        except KeyError as err:
            self.__log_and_raise_file_sect(err)

    def xdc_sources(self) -> list[str]:
        '''Get list of pathnames of constraint files'''
        try:
            return self.files_sect()['xdc']
        except KeyError as err:
            self.__log_and_raise_file_sect(err)

    def bd_sources(self) -> list[str]:
        '''Get list of pathnames of constraint files'''
        try:
            return self.files_sect()['bd']
        except KeyError as err:
            self.__log_and_raise_file_sect(err)

###############################################################################
# Simulator configs
###############################################################################
class SimConfigs(VivadoInstrsCfgBase):
    '''Sim cfg'''

    def __init__(
          self
        , logger
        , yaml_sect
        ):
        super().__init__(logger, yaml_sect)

    def top_module(self) -> list[str]:
        try:
            return self.yaml_obj['top_module_name']
        except KeyError as err:
            self.__log_and_raise(err)

    def hdl_files(self) -> list[str] | None:
        try:
            return self.files_sect()['hdl']
        except KeyError as err:
            self.__log_and_raise(err)

    def used_simlibs(self) -> list[str]:
        try:
            return self.yaml_obj['included_libs']
        except KeyError as err:
            self.__log_and_raise(err)

    def wdb_file_pname(self) -> list[str]:
        try:
            return self.yaml_obj['wdb_file']
        except KeyError as err:
            self.__log_and_raise(err)

    def wcfg_file_pname(self) -> list[str]:
        try:
            return self.yaml_obj['wcfg_file']
        except KeyError as err:
            self.__log_and_raise(err)

class ConfigGetter:

    def __init__(
          self
        , logger
        , yaml_pname
        ):

        self.logger = logger
        with open(yaml_pname, 'r', encoding = "utf-8") as file:
            yaml_obj = yaml.safe_load(file)

        self.sim     = SimConfigs(
                              logger
                            , yaml_obj['sim']
                            )

        self.vivado  = VivadoConfigs(
                              logger
                            , yaml_obj['vivado']
                            )
        self.common  = CommonConfigs(
                              logger
                            , yaml_obj['common']
                            )
