import os

import aux.exceptions as exceptions
import configs.arg_parser as arg_parser
import configs.config_getter as config_getter
import launch_env
import vivado
import sim
from gpop_actions import GenPopParams
import aux.futils as futils
from aux.term_utils import TermInterractor

class Params:
    def __init__(self):
        self.main: GenPopParams | None   = None
        self.applic: vivado.VivadoRunnerConfig | sim.SimRunnerConfig | None = None

class Parameterizer:
    '''
    '''

    def __init__(
          self
        , logger
        , mroot_dir
        ):
        self.logger = logger
        self.futils = futils.FUtils(logger = logger)
        self.mroot_dir = mroot_dir
        self.params = Params()

        # args parse
        self.argp = arg_parser.ArgParser(
                              "run"
                            , "Project build/modeling start script"
                            , self.logger
                            )
        self.args = self.argp.args()

        if self.args.main.create_cfg_path:
            path = os.path.realpath(self.args.main.create_cfg_path)
            self._create_user_cfg_templ(path)
            return

        user_conf = self.args.main.conffile
        if None == user_conf:
            self.logger.error(
                f"It is required to specify a user config file"
                )
            raise exceptions.BadConfig()

        # read configs
        self.cfg = config_getter.ConfigGetter(
              self.logger
            , user_conf
            )

        self.params.main = GenPopParams()
        self.params.main.vivado_bindir_pname = self.cfg.common.vivado_bindir_pname()
        self.params.main.used_config_pname = user_conf
        self.params.main.main_prj_path = os.path.dirname(user_conf)

        if type(self.args.subr) is arg_parser.VivadoArgs:
            self.params.applic = vivado.VivadoRunnerConfig()
            self.params.applic.fpga_partnum     = self.cfg.vivado.fpga_partnum()
            self.params.applic.vdefs            = self.cfg.vivado.vdefs()
            self.params.applic.top_module_name  = self.cfg.vivado.top_module_name()
            self.params.applic.hdl_src          = self.cfg.vivado.hdl_sources()
            self.params.applic.constr_src       = self.cfg.vivado.xdc_sources()
            self.params.applic.bd_src           = self.cfg.vivado.bd_sources()
            self.params.applic.hw_server_ip     = self.cfg.vivado.hwserver_ip()
            self.params.applic.hw_server_port   = self.cfg.vivado.hwserver_port()
            self.params.applic.jtag_freq_hz     = self.cfg.vivado.jtag_freq_hz()

            self.params.applic.run_lint         = self.args.subr.run_lint
            self.params.applic.run_synth        = self.args.subr.run_synth
            self.params.applic.run_reroute      = self.args.subr.run_reroute
            self.params.applic.run_pnr          = self.args.subr.run_pnr
            self.params.applic.run_all          = self.args.subr.run_all
            self.params.applic.mk_bin           = self.args.subr.mk_bin
            self.params.applic.flash            = self.args.subr.flash
            self.params.applic.run_gui          = self.args.subr.run_gui
            self.params.applic.run_tcl          = self.args.subr.run_tcl
            self.params.applic.gen_bd           = self.args.subr.gen_bd
            self.params.applic.postsynth_cmd    = self.args.subr.postsynth_cmd
            self.params.applic.postplace_cmd    = self.args.subr.postplace_cmd
            self.params.applic.postroute_cmd    = self.args.subr.postroute_cmd


        if type(self.args.subr) is arg_parser.SimArgs:
            self.params.applic = sim.SimRunnerConfig()
            self.params.applic.top_module_name  = self.cfg.sim.top_module()
            self.params.applic.hdl_sources      = self.cfg.sim.hdl_files()
            self.params.applic.used_simlibs     = self.cfg.sim.used_simlibs()
            self.params.applic.wdb_name         = self.cfg.sim.wdb_file_pname()
            self.params.applic.wcfg_name        = self.cfg.sim.wcfg_file_pname()

            self.params.applic.use_gui          = self.args.subr.run_gui


        if self.params.main == None and self.params.applic == None:
            self.logger.error(
                "Error during parameter parsing, wrong type of script being run"
                )
            raise exceptions.BadConfig()

    def _create_user_cfg_templ(self, target_path: str) -> None:
        templ = os.path.join(
              self.mroot_dir
            , launch_env.user_cfg_template
            )
        if not os.path.isfile(templ):
            self.logger.error(
                f"Template file ({templ}) not found"
                )
            raise exceptions.BadConfig()

        usr_cfg = os.path.join(
              target_path
            , launch_env.user_conf_fname
            )
        ret = True
        if os.path.isfile(usr_cfg):
            ret = TermInterractor.qst_prio(
                f"Warning! A request to create a custom config has detected "
                f"that a custom config ({usr_cfg}) already exists. Do you "
                f"want to overwrite it? "
                )
        if ret:
            ret = self.futils.copy_file(
                  src  = templ
                , dest = usr_cfg
                )
            if not ret:
                raise exceptions.BadConfig()
