"""Program command line arguments parsing module"""

import argparse
import aux.logger as logger

import launch_env

class VivadoArgs():
    '''
    Storage structure of artugments related to the launch of
    synthesis/implementation tools
    '''

    def __init__(
          self
        , run_lint      : bool
        , run_synth     : bool
        , run_reroute   : bool
        , run_pnr       : bool
        , run_all       : bool
        , mk_bin        : bool
        , flash         : bool
        , run_gui       : bool
        , run_tcl       : bool
        , gen_bd        : bool
        , postsynth_cmd : list[str] | None = None
        , postplace_cmd : list[str] | None = None
        , postroute_cmd : list[str] | None = None
        ):
        self.run_lint       = run_lint
        self.run_synth      = run_synth
        self.run_reroute    = run_reroute
        self.run_pnr        = run_pnr
        self.run_all        = run_all
        self.mk_bin         = mk_bin 
        self.flash          = flash
        self.run_gui        = run_gui
        self.run_tcl        = run_tcl
        self.gen_bd         = gen_bd
        self.postsynth_cmd  = postsynth_cmd
        self.postplace_cmd  = postplace_cmd
        self.postroute_cmd  = postroute_cmd

class SimArgs():
    '''
    Storage structure of artugments related to the launch of simulation tools
    '''

    def __init__(
          self
        , run_gui
        ):
        self.run_gui = run_gui


class MainArgs():
    '''
    Structure for main args storage
    '''

    def __init__(
          self
        , create_cfg_path   : str
        , conffile          : str
        ):

        self.create_cfg_path = create_cfg_path
        self.conffile        = conffile

class OutArgContainer:

    def __init__(self):
        self.main : MainArgs = None
        self.subr : Args     = None

class ArgParser:
    """Arg parser
    """

    _subparser_key          = 'subparser'
    """
    key which after parsing the arguments will point to the used subparser
    """


    def __init__(self, progname, descr_msg, logger):
        """
        Argument parser
        ------------------

        The parser parses and stores the arguments into a structure returned to
        the user. to the user. Parsing takes place in the constructor. All that
        is necessary for parsing is to create a parser object and get data from
        it

        :param progname:   program name printed in the help;
        :param descr_msg:  message describing the program (needed for the help)
        """

        self.logger     = logger
        self.viv_parser_name = 'vivado'
        self.viv_parser_alias = ('viv')
        self.sim_parser_name = 'sim'

        # ----------- main configs ------------
        self._parser = argparse.ArgumentParser(
            # prog specifies a name. If the argument is left untouched, the name
            # will be the file name
            prog = progname,
            description = descr_msg,
            formatter_class = argparse.RawDescriptionHelpFormatter,
            )
        self._subparsers = self._parser.add_subparsers(
              title = "Subprogramms"
              # this thing allows you to set the name of a variable into which
              # will be written the name of the current sub-parser when parsing
            , dest  = self._subparser_key
            , required = False
            )
        # ----------- main args ------------
        self._parser.add_argument(
              '-C', '--create_conf'
            , help = 'Generate a template file at the specified path'
            , required = False
            )
        self._parser.add_argument(
              '-c', '--conf'
            , help = f"Path to the config file. By default it will be used "
                     f"\"{launch_env.user_conf_fname}\""
            , required = False
            )

        # ----------- subparser configs ------------
        self._add_vivado_parser()
        self._add_sim_parser()

        # ----------- run parser ------------
        self._args = self._parser.parse_args()

        self.out_args = OutArgContainer()

        self.out_args.main = self._save_main_args()
        # check which sub-parser the arguments came to, call its function
        # save arguments
        if None != self._args.subparser:
            if self._args.subparser in (self.viv_parser_name) + self.viv_parser_alias:
                self.out_args.subr = self._save_vivado_args()

            if self._args.subparser in self.sim_parser_name:
                self.out_args.subr = self._save_sim_args()

    def args(self) -> OutArgContainer:
        """Return args"""
        return self.out_args

    def _save_main_args(self) -> MainArgs:
        return MainArgs(
              create_cfg_path   = self._args.create_conf
            , conffile          = self._args.conf
            )

    #
    # Vivado args parsing
    #
    def _add_vivado_parser(self):
        """
        Add parser for vivado args
        """
        self._my_parser = self._subparsers.add_parser(
              self.viv_parser_name
            , aliases = [self.viv_parser_alias]
            , help = "Vivado argumets group"
            )
        self._my_parser.add_argument(
              '-l', '--lint'
            , help = f"run linter"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '-s', '--synthesis'
            , help = f"run synthesis"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '-r', '--reroute'
            , help = f"run re-rounting. RRead the synth checkpoint, and "
                     f"perform all further steps to build the project"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '-p', '--pnr'
            , help = f"run placement and rounting"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '-b', '--bin'
            , help = f"make bin artefacts"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '-a', '--all'
            , help = f"run full prj building"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '-f', '--flash'
            , help = f"flash FPGA"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '-g', '--gui'
            , help = f"run Vivado GUI"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '-t', '--term'
            , help = f"run Vivado TCL terminal"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '--gen_bd'
            , help = f"Generate HDL from BlockDesign"
            , action = 'store_true'
            , required = False
            )
        self._my_parser.add_argument(
              '--postsynth_cmd'
            , help = f"Load post synthesis checkpoint and run command"
            , required = False
            , nargs = '+'
            )
        self._my_parser.add_argument(
              '--postplace_cmd'
            , help = f"Load post place checkpoint and run command"
            , required = False
            , nargs = '+'
            )
        self._my_parser.add_argument(
              '--postroute_cmd'
            , help = f"Load post route checkpoint and run command"
            , required = False
            , nargs = '+'
            )

    def _save_vivado_args(self) -> VivadoArgs:
        return VivadoArgs(
              run_lint      = self._args.lint
            , run_synth     = self._args.synthesis
            , run_reroute   = self._args.reroute
            , run_pnr       = self._args.pnr
            , run_all       = self._args.all
            , mk_bin        = self._args.bin
            , flash         = self._args.flash
            , run_gui       = self._args.gui
            , run_tcl       = self._args.term
            , gen_bd        = self._args.gen_bd
            , postsynth_cmd = self._args.postsynth_cmd
            , postplace_cmd = self._args.postplace_cmd
            , postroute_cmd = self._args.postroute_cmd
            )

    #
    # Sim args parsing
    #
    def _add_sim_parser(self):
        """
        Добавить парсер для группппы аргументов клиента
        """
        self._my_parser = self._subparsers.add_parser(
              'sim'
            , help = "Simulator args group"
            )
        self._my_parser.add_argument(
              '-g', '--gui'
            , help = "run GUI mode"
            , action = 'store_true'
            , required = False
            )
    def _save_sim_args(self) -> SimArgs:

        return SimArgs(
            run_gui = self._args.gui
            )

