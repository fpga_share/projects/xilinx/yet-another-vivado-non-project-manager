import os
import re

import launch_env
from aux.prog_runner import ProgRunner
from src_searcher import SrcSearcher
from gpop_actions import GenPopParams
from aux.term_utils import TermColors
import aux.exceptions as exceptions

###############################################################################
# Sim run configs
###############################################################################
class SimRunnerConfig:
    top_module_name : str
    hdl_sources : list[str]
    used_simlibs : list[str]
    use_gui : bool
    wdb_name : str
    wcfg_name : str

class _CmdBook:

    def __init__(
          self
        , proot_dir             : str
        , vivado_bindir_pname   : str
        ):

        self.xvlog_pname = os.path.join(
                                  # root_dir
                                  vivado_bindir_pname
                                , "xvlog"
                                )
        self.xelab_pname = os.path.join(
                                  # root_dir
                                  vivado_bindir_pname
                                , "xelab"
                                )
        self.xsim_pname = os.path.join(
                                  # root_dir
                                  vivado_bindir_pname
                                , "xsim"
                                )

        self.log_d = os.path.join(proot_dir, launch_env.xsim_logs_dir)
        self.arts_d = os.path.join(proot_dir, launch_env.xsim_arts_dir)

    def mk_prj(self, vlist : list[str], svlist : list[str]) -> str:
        cmd = ""

        if len(vlist):
            cmd += "verilog "
        cmd += f"{launch_env.xsim_worklib} "
        for item in vlist:
            cmd += f"{item} \\\n"
        cmd += "\n"

        if len(svlist):
            cmd += "sv "
        cmd += f"{launch_env.xsim_worklib} "
        for item in svlist:
            cmd += f"{item} \\\n"
        cmd += "\n"
        cmd += "nosort"

        return cmd

    def compile_cmd(self, simprj_pname) -> str:
        cmd = f"{self.xvlog_pname} "

        cmd += f"--incr --relax -L uvm -prj {simprj_pname} "
        cmd += f"-log {self.log_d}/xvlog.log "

        return cmd

    def run_elaborate(
          self
        , top_module_name: str
        , sim_list: list[str]
        ) -> str:
        ret = f"{self.xelab_pname}      \
        --incr                          \
        --debug typical                 \
        --relax                         \
        --mt auto                       \
        -L {launch_env.xsim_worklib}    \
        -log {self.log_d}/xelab.log     \
        -s {top_module_name}            \
        --timescale 1ns/1ns \
        "
        
        for lib in sim_list:
            ret += f"-L {lib} "
        ret += f"{launch_env.xsim_worklib}.{top_module_name} "
        ret += f"{launch_env.xsim_worklib}.glbl "

        return ret

    def run_sim(
          self
        , top_module_name: str
        , use_gui: bool 
        , wdb_pname: str | None = None
        , wcfg_pname: str | None = None
        ) -> str:

        cmd = f"{self.xsim_pname}       \
        {top_module_name}               \
        -log {self.log_d}/xsim.log      \
        "
        if use_gui:
            cmd += "-gui "
        else:
            cmd += "-onfinish quit -onerror quit -runall "
        if None != wdb_pname:
            cmd += f"-wdb {wdb_pname} "
        if None != wcfg_pname:
            cmd += f"-view {wcfg_pname} "

        return cmd


class Sim:

    def __init__(
          self
        , logger
        , sim_params    : SimRunnerConfig
        , main_params   : GenPopParams
        ):
        self.logger     = logger
        self.proot_dir  = main_params.main_prj_path
        self.sim_params = sim_params
        self.main_params= main_params
        self.runner     = ProgRunner(logger)
        self.src_srchr  = SrcSearcher(logger, self.proot_dir)
        self.xsim_d     = os.path.join(self.proot_dir, launch_env.xsim_dir)
        self.simprj_pname = os.path.join(
                                  self.xsim_d
                                , f"{self.sim_params.top_module_name}.prj"
                                )

        self.cmdbook = _CmdBook(
                      proot_dir = self.proot_dir
                    , vivado_bindir_pname = self.main_params.vivado_bindir_pname
                    )

    def _colorize_log(self, line : str) -> str:
        termclr = TermColors()

        info_str = "INFO:"
        clr_info_str = termclr.green_bold_str(info_str)
        line = re.sub(info_str, clr_info_str, line)

        warn_str = "WARNING:"
        clr_warn_str = termclr.blue_bold_str(warn_str)
        line = re.sub(f"^{warn_str}", clr_warn_str, line)

        crit_warn_str = "CRITICAL WARNING:"
        clr_critwarn_str = termclr.violet_bold_str(crit_warn_str)
        line = re.sub(crit_warn_str, clr_critwarn_str, line)

        err_str = "ERROR:"
        clr_err_str = termclr.red_bold_str(err_str)
        line = re.sub(err_str, clr_err_str, line)

        err_str = "Error:"
        clr_err_str = termclr.red_bold_str(err_str)
        line = re.sub(err_str, clr_err_str, line)

        return line

    def _run_xsim(
          self
        , run_cmd : str
        , need_run_in_backgr : bool = False
        , new_cwd : str | None = None
        ):
        if need_run_in_backgr:
            runner = self.runner.run_sytem_cmd_detached
        else:
            self.runner.set_colorizer(self._colorize_log)
            runner = self.runner.run_sytem_cmd

        ret = runner(cmd = run_cmd, cwd = new_cwd)
        if need_run_in_backgr:
            self.logger.info("Wait a few seconds for the GUI to start...")
            return 0
        else:
            if ret[0]:
                self.logger.error(
                    f"Xsim terminated with an error!\n"
                    )
            return ret[0]


    def _compile_src(self):
        self.logger.info("Run src parsing")
        v_list, sv_list = self.src_srchr.create_hdl_files_lists(
                                                    self.sim_params.hdl_sources
                                                    )

        if 0 == len(v_list) and 0 == len(sv_list):
            self.logger.error(
                f"No source for the build is specified in the configuration "
                f"file!"
                )
            raise exceptions.BadOp()

        prj_file_txt = self.cmdbook.mk_prj(
                              vlist = v_list
                            , svlist = sv_list
                            )

        with open(self.simprj_pname, 'w') as file:
            file.write(prj_file_txt)

        cmd = self.cmdbook.compile_cmd(self.simprj_pname)

        ret = self._run_xsim(cmd, False, self.xsim_d)
        return ret

    def _elab(self):
        self.logger.info("Run Elaboration")
        cmd = self.cmdbook.run_elaborate(
              top_module_name   = self.sim_params.top_module_name
            , sim_list          = self.sim_params.used_simlibs
            )
        ret = self._run_xsim(cmd, False, self.xsim_d)
        return ret

    def _simulate(self):
        self.logger.info("Run Simulation")

        wdb = None
        wcfg = None
        if "" != self.sim_params.wdb_name:
            wdb = os.path.join(self.proot_dir, self.sim_params.wdb_name)
        if "" != self.sim_params.wcfg_name:
            wcfg = os.path.join(self.proot_dir, self.sim_params.wcfg_name)


        cmd = self.cmdbook.run_sim(
              top_module_name   = self.sim_params.top_module_name
            , use_gui           = self.sim_params.use_gui
            , wdb_pname         = wdb
            , wcfg_pname        = wcfg
            )
        ret = self._run_xsim(
                          cmd
                        , self.sim_params.use_gui
                        , self.xsim_d
                        )
        return ret

    def run(self):
        ret = self._compile_src()
        if ret:
            return ret
        self._elab()
        if ret:
            return ret
        return self._simulate()
